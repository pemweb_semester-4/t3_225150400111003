document.getElementById('myForm').addEventListener('submit', function(event) {
    event.preventDefault(); // Prevent the default form submission

    var form = event.target;
    var formData = new FormData(form);

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'process.php', true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            // Handle successful response
            document.getElementById('response').innerHTML = xhr.responseText;
            // Display greeting message using JavaScript
            var responseText = xhr.responseText;
            var greeting = responseText.match(/<h2>(.*?)<\/h2>/)[1]; // Extract greeting message
            alert(greeting);
        }
    };
    xhr.send(formData);
});
